
from datetime import datetime

from servant import get, post, staticfiles

@get('/', permissions='PUBLIC')
def index(ctx):
    route = staticfiles.route_from_name('static')
    return route.get('index.html')

@post('/click', permissions='PUBLIC')
def click(ctx, counter, timestamp):
    print('COUNTER:', counter, type(counter))
    print('TIMESTAMP:', timestamp, type(timestamp))

    return {
        'counter': counter + 2,
        'timestamp': datetime.now()
    }
